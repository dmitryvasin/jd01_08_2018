package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsOne;
import by.itacademy.lesson6.operations.SquareRoot;

public class MenuSquareRoot extends MenuCommonOperation implements MenuItem {
    public MenuSquareRoot(RootMenuItem rootMenu) {
        super(new SquareRoot(), rootMenu);
    }

    @Override
    protected int operandsCount() {
        return 1;
    }

    @Override
    protected Operands operandsType(double... abc) {
        return new OperandsOne(abc[0]);
    }

    @Override
    public String name() {
        return "SquareRoot";
    }
}
