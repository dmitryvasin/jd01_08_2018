package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;
import by.itacademy.lesson6.operations.Operation;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MenuCommonOperation implements MenuItem {
    private static final Logger LOGGER = Logger.getLogger(MenuCommonOperation.class.getName());

    private static final Scanner SCANNER = new Scanner(System.in);

    private Operation operation;
    private RootMenuItem rootMenuItem;

    public MenuCommonOperation(Operation operation, RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
        this.operation = operation;
    }

    @Override
    public void execute() {
        double arguments[] = new double[operandsCount()];
        for(int i = 0; i < operandsCount(); i++){
            System.out.println("Введите операнд: ");
            arguments[i] = SCANNER.nextDouble();
        }

        try {
            Operands operandsTwo = operandsType(arguments);
            System.out.println(operation.calculate(operandsTwo));
        } catch (OperandsBoundsException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }

        rootMenuItem.execute();
    }

    protected abstract int operandsCount();

    protected abstract Operands operandsType(double... abc);
}


