package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsOne;
import by.itacademy.lesson6.operations.Exhibitor;

public class MenuExhibitor extends MenuCommonOperation implements MenuItem {
    public MenuExhibitor(RootMenuItem rootMenu) {
        super(new Exhibitor(), rootMenu);
    }

    @Override
    protected int operandsCount() {
        return 1;
    }

    @Override
    protected Operands operandsType(double... abc) {
        return new OperandsOne(abc[0]);
    }

    @Override
    public String name() {
        return "Exhibitor";
    }
}
