package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsTwo;
import by.itacademy.lesson6.operations.Multiply;

public class MenuMultiply extends MenuCommonOperation implements MenuItem {
    public MenuMultiply(RootMenuItem rootMenu) {
        super(new Multiply(), rootMenu);
    }

    @Override
    protected int operandsCount() {
        return 2;
    }

    @Override
    protected Operands operandsType(double... abc) {
        return new OperandsTwo(abc[0], abc[1]);
    }

    @Override
    public String name() {
        return "Multiply";
    }
}
