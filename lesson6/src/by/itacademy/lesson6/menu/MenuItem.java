package by.itacademy.lesson6.menu;

public interface MenuItem {
    void execute();

    String name();
}
