package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsTwo;
import by.itacademy.lesson6.operations.Subtraction;

public class MenuSubtraction extends MenuCommonOperation implements MenuItem {
    public MenuSubtraction(RootMenuItem rootMenu) {
        super(new Subtraction(), rootMenu);
    }

    @Override
    protected int operandsCount() {
        return 2;
    }

    @Override
    protected Operands operandsType(double... abc) {
        return new OperandsTwo(abc[0], abc[1]);
    }

    @Override
    public String name() {
        return "Subtraction";
    }
}
