package by.itacademy.lesson6;

import by.itacademy.lesson6.menu.MenuDisplay;
import by.itacademy.lesson6.menu.RootMenuItem;

public class SimpleCalculator {
    private RootMenuItem rootMenu = new MenuDisplay();

    public void execute() {
        rootMenu.execute();
    }
}
