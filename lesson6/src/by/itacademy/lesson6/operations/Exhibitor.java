package by.itacademy.lesson6.operations;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;

public class Exhibitor implements Operation {
    @Override
    public double calculate(Operands operands) throws OperandsBoundsException {
        return Math.exp(operands.get(0));
    }
}
