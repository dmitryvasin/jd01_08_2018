package by.itacademy.lesson6.operations;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;

public class Division implements Operation {
    @Override
    public double calculate(Operands operands) throws OperandsBoundsException {
        return operands.get(0) / operands.get(1);
    }
}
