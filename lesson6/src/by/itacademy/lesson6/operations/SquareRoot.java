package by.itacademy.lesson6.operations;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;

public class SquareRoot implements Operation {
    @Override
    public double calculate(Operands operands) throws OperandsBoundsException {
        return Math.sqrt(operands.get(0));
    }
}
