package by.itacademy.lesson5.dateable;

public interface Input {
    void read();
}
