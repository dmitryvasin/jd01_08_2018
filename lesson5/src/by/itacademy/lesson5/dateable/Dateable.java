package by.itacademy.lesson5.dateable;

import java.util.Date;

public interface Dateable {
    Date date();
}
