package by.itacademy.lesson5.dateable;

public interface CalculationAverageAge {
    TimePeriod execute(Dateable[] list);
}
