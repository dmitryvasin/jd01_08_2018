package by.itacademy.lesson5.dateable;

public interface AverageAge {
    TimePeriod calculate();
}
