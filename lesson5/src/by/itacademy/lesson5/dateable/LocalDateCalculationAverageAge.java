package by.itacademy.lesson5.dateable;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class LocalDateCalculationAverageAge implements CalculationAverageAge {
    @Override
    public TimePeriod execute(Dateable[] list) {
        int yearsSum = 0, monthsSum = 0, daysSum = 0;
        LocalDate now = LocalDate.now();

        for (Dateable item : list) {
            LocalDate birthDate =  item.date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Period period = Period.between(birthDate, now);
            yearsSum += period.getYears();
            monthsSum += period.getMonths();
            daysSum += period.getDays();
        }

        return new TimePeriod(yearsSum/list.length, monthsSum/list.length, daysSum/list.length);
    }
}