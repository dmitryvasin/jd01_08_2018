package by.itacademy.lesson5.dateable;

import java.util.Calendar;
import java.util.Date;

public class CalendarCalculationAverageAge implements CalculationAverageAge {
    @Override
    public TimePeriod execute(Dateable[] list) {
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        AveragePeriod  period = new AveragePeriod(list);
        int years = period.years();
        int month = period.months();
        int days = period.days();

        return new TimePeriod(years, month, days);
    }

    private class AveragePeriod {
        private Calendar now = Calendar.getInstance();
        private Dateable[] list;

        AveragePeriod(Dateable[] list) {
            this.list = list;
        }

        int years() {
            int yearsSum = 0;
            for (Dateable item : list) {
                Calendar birthDate = Calendar.getInstance();
                birthDate.setTime(item.date());

                yearsSum += yearsDifference(now, birthDate);
            }

            return yearsSum / list.length;
        }

        int months() {
            int monthsSum = 0;
            for (Dateable item : list) {
                Calendar birthDate = Calendar.getInstance();
                birthDate.setTime(item.date());

                monthsSum += monthDifference(now, birthDate);
            }

            return monthsSum / list.length;
        }

        int days() {
            int daysSum = 0;

            for (Dateable student : list) {
                Calendar birthDate = Calendar.getInstance();
                birthDate.setTime(student.date());

                daysSum += daysDifference(now, birthDate);
            }

            return daysSum / list.length;
        }

        private int yearsDifference(Calendar current, Calendar past) {
            int daysDifference = current.get(Calendar.DAY_OF_YEAR) - past.get(Calendar.DAY_OF_YEAR);
            int years = current.get(Calendar.YEAR) - past.get(Calendar.YEAR);
            return daysDifference > 0 ? years : years - 1;
        }

        private int monthDifference(Calendar current, Calendar past) {
            Calendar nowWithoutYears = (Calendar) current.clone();
            nowWithoutYears.add(Calendar.YEAR, -yearsDifference(nowWithoutYears, past));

            int months = 0;
            while (past.before(nowWithoutYears)) {
                months++;
                nowWithoutYears.add(Calendar.MONTH, -1);
            }
            return months - 1;
        }

        private int daysDifference(Calendar current, Calendar past) {
            Calendar nowWithoutYearsAndMonths = (Calendar) current.clone();

            int years = yearsDifference(current, past);
            int months = monthDifference(current, past);

            nowWithoutYearsAndMonths.add(Calendar.YEAR, -years);
            nowWithoutYearsAndMonths.add(Calendar.MONTH, -months);

            int days = 0;
            if (past.after(nowWithoutYearsAndMonths)) {
                Calendar temp = past;
                past = nowWithoutYearsAndMonths;
                nowWithoutYearsAndMonths = temp;
            }

            while (past.before(nowWithoutYearsAndMonths)) {
                days++;
                nowWithoutYearsAndMonths.add(Calendar.DAY_OF_MONTH, -1);
            }

            return days - 1;
        }
    }
}