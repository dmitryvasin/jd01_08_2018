package by.itacademy.lesson5.atm;

public interface Income {
    void add(int cash);
}
