package by.itacademy.lesson5.atm;

public class PriorATM extends ATM implements Income, Outcome, VendorInfo  {
    @Override
    public String display() {
        return "Prior";
    }
}