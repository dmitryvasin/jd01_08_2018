package by.itacademy.lesson5.atm;

public class BelarusATM extends ATM implements Income, Outcome, VendorInfo {
    @Override
    public String display() {
        return "Belarus Bank";
    }
}