package by.itacademy.lesson5.atm;

public interface VendorInfo {
    String display();
}
