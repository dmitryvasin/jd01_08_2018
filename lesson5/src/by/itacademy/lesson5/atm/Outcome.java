package by.itacademy.lesson5.atm;

public interface Outcome {
    void cashOut(int sum);
}
