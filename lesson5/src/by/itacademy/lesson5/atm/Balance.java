package by.itacademy.lesson5.atm;

public interface Balance {
    int total();
}
