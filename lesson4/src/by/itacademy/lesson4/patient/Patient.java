package by.itacademy.lesson4.patient;

public class Patient {
    private String name;
    private int age;
    private boolean illness;

    public Patient(String name, int age, boolean illness) {
        this.name = name;
        this.age = age;
        this.illness = illness;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("Пациент - ").append(name).append("; ");
        out.append("Возраст - ").append(age).append("; ");
        out.append("Болеет ли - ").append(illness).append(".");
        return out.toString();
    }
}
