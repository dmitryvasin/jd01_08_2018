public class ArraySameElements {
    public static void main(String[] args) {
        int[] array = {2, 3, 4, 7, 6, 5, 7, 3, 7, 20, 7, 7, 5};
        boolean[] checked = new boolean[array.length];
        int count = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j] && !checked[j]) {
                    count++;
                    checked[j] = true;
                }
            }
            if (count > 1) {
                System.out.println(array[i] + " повторяется " + count);
                count = 1;
            }
        }
    }
}
