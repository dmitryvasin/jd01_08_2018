public class NextDay {
    public static void main(String[] args) {
        int day = 28, month = 2, year = 2017;
        if (month == 12) {
            if (day == 31) {
                year++;
                month = 1;
                day = 1;
            }
        } else if (month == 1 || month == 3 || month == 5 || month == 7
                || month == 8 || month == 10)
            if (day == 31) {
                day = 1;
                month++;
            } else day++;
        else if (month == 4 || month == 6 || month == 9 || month == 11)
            if (day == 30) {
                month++;
                day = 1;
            } else day++;
        else if (month == 2)
            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
                if (day == 29) {
                    month++;
                    day = 1;
                } else day++;
            else {
                if (day == 28) {
                    month++;
                    day = 1;
                } else day++;
            }
        System.out.println("Date:" + day + " " + month + " " + year);
    }


}