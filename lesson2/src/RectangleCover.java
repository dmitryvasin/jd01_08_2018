public class RectangleCover {
    public static void main(String[] args) {
        int width = 6, length = 8, radius = 5;
        double expectedRadius = Math.sqrt(width * width + length * length) / 2;
        if (expectedRadius <= radius)
            System.out.println("Отверстие перекрыто");
        else
            System.out.println("Увеличить радиус");
    }
}