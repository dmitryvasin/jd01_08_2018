public class Substring {
    public static void main(String[] args) {
        String str = "public static void";
        int length = str.length();
        System.out.println("Количество символов в строке: " + length);
        String firstHalf = str.substring(0, length / 2);
        String secondHalf = str.substring(length / 2);
        System.out.println(firstHalf);
        System.out.println(secondHalf);
    }

}
