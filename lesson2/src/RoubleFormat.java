public class RoubleFormat {
    public static void main(String[] args) {
        int money = 1500;
        int remainder100 = money % 100;
        int remainder10 = money % 10;
        String rouble;
        if (remainder10 == 1 && remainder100 != 11)
            rouble = "рубль";
        else if (remainder10 >= 2 && remainder10 <= 4 && remainder100 != 12 && remainder100 != 13 && remainder100 != 14)
            rouble = "рубля";
        else
            rouble = "рублей";
        System.out.println(money + " " + rouble);
    }
}
